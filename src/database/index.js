import Sequelize from 'sequelize'
import mysql from 'mysql2'
import Debug from 'debug'
import { SEQUELIZE_DIALECTS } from '../constants'
import { initModels, initAssociations, loadFixtures } from '../models'

const debug = Debug('course-signup')

let sequelize

export default async () => {
	if (sequelize && sequelize) {
		// ensure client is connected
		await sequelize.authenticate()

		return sequelize
	}

	const isLoggingEnabled = [true, 'true'].includes(process.env.SEQUELIZE_LOGGING)
	/* istanbul ignore next */
	const logging = isLoggingEnabled ? message => debug(message) : false

	/* istanbul ignore next */
	debug('connecting to database - %j', {
		dialect: process.env.SEQUELIZE_DIALECT,
		host: process.env.SEQUELIZE_HOST,
		port: process.env.SEQUELIZE_PORT,
		database: process.env.SEQUELIZE_DATABASE,
		user: process.env.SEQUELIZE_USER,
		pass: process.env.SEQUELIZE_PASS && '*****'
	})

	if (process.env.SEQUELIZE_DIALECT === SEQUELIZE_DIALECTS.MYSQL) {
		sequelize = new Sequelize(
			process.env.SEQUELIZE_DATABASE,
			process.env.SEQUELIZE_USER,
			process.env.SEQUELIZE_PASS,
			{
				host: process.env.SEQUELIZE_HOST,
				port: process.env.SEQUELIZE_PORT,
				dialect: 'mysql',
				dialectModule: mysql,
				logging,
				pool: {
					min: 0,
					max: 15,
					idle: 15000,
					acquire: 30000
				},
				dialectOptions: {
					connectTimeout: 30000
				}
			}
		)
	} else if (process.env.SEQUELIZE_DIALECT === SEQUELIZE_DIALECTS.SQLITE) {
		// eslint-disable-next-line import/no-extraneous-dependencies, global-require
		const sqlite = require('sqlite3')

		sequelize = new Sequelize('sqlite::memory:', {
			dialectModule: sqlite,
			logging
		})
	} else {
		throw new Error('unsupported or undefined sequelize dialect')
	}

	await sequelize.authenticate()

	debug('connected to database')

	initModels({ sequelize })
	initAssociations()

	debug('syncing database schema')
	await sequelize.sync()

	await loadFixtures()

	return sequelize
}

export const disconnect = async () => {
	if (sequelize) {
		await sequelize.close()
	}

	sequelize = undefined
}
