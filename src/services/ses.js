import AWS from 'aws-sdk'
import Debug from 'debug'

const debug = Debug('course-signup')

export default async ({ recipient: recipientRaw, subject, message }) => {
	try {
		const ses = new AWS.SESV2({
			region: process.env.SES_REGION,
			apiVersion: '2019-09-27'
		})

		if (!['dev', 'prod'].includes(process.env.STAGE)) {
			// eslint-disable-next-line no-console
			console.log(
				'not sending email on purpose\n' +
				`sender: ${process.env.SES_SENDER}\n` +
				`recipient: ${recipientRaw}\n` +
				`subject: ${subject}\n` +
				`message: ${message}`
			)

			return
		}

		const recipient = process.env.STAGE !== 'prod' ? process.env.SES_FAILSAFE_RECIPIENT : recipientRaw
		if (recipient === process.env.SES_FAILSAFE_RECIPIENT) {
			// eslint-disable-next-line no-console
			console.log('using ses failsafe recipient')
		}

		const response = await ses.sendEmail({
			Content: {
				Simple: {
					Body: {
						Html: {
							Data: message.split('\n').join('<br/>')
						},
						Text: {
							Data: message
						}
					},
					Subject: {
						Data: subject
					}
				}
			},
			Destination: {
				ToAddresses: [recipient]
			},
			FromEmailAddress: process.env.SES_SENDER
		}).promise()

		debug('sending of email result - %j', response)
	} catch (error) {
		/* istanbul ignore next */
		debug('sending of email failed - %j', error)
	}
}
