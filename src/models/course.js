import { Model, DataTypes } from 'sequelize'

export class Course extends Model {}

export const initModel = ({ sequelize }) => {
	Course.init(
		{
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			subject: {
				type: DataTypes.STRING,
				allowNull: false
			},
			seats: {
				type: DataTypes.INTEGER,
				validate: {
					min: 1
				}
			},
			seatsAvailable: {
				type: DataTypes.VIRTUAL(DataTypes.INTEGER, ['seats']),
				get() {
					return Math.max(
						0,
						this.get('seats') - (this.students && this.students.length) || 0
					)
				}
			}
		},
		{ sequelize, tableName: 'courses', modelName: 'courses' }
	)
}

export const initAssociations = ({ Student }) => {
	Course.belongsToMany(Student, { through: 'CourseStudent' })
}
