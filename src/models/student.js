import { Model, DataTypes } from 'sequelize'

export class Student extends Model {}

export const initModel = ({ sequelize }) => {
	Student.init(
		{
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			name: {
				type: DataTypes.STRING,
				allowNull: false
			},
			surname: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true,
				validate: {
					isEmail: true,
					// eslint-disable-next-line no-useless-escape
					is: [`.+@${process.env.EMAIL_DOMAIN.replace('.', '.')}`]
				}
			},
			matriculation: {
				type: DataTypes.INTEGER,
				allowNull: false,
				unique: true
			}
		},
		{ sequelize, tableName: 'students', modelName: 'students' }
	)
}

export const initAssociations = ({ Course }) => {
	Student.belongsToMany(Course, { through: 'CourseStudent' })
}
