import Debug from 'debug'
import {
	Course,
	initModel as initCourseModel,
	initAssociations as initCourseAssociations
} from './course'
import {
	Student,
	initModel as initStudentModel,
	initAssociations as initStudentAssociations
} from './student'

const fs = require('fs')

const debug = Debug('course-signup')

const models = {
	Course,
	Student
}

export const initModels = ({ sequelize }) => {
	debug('initializing course model')
	initCourseModel({ sequelize })

	debug('initializing student model')
	initStudentModel({ sequelize })
}

export const initAssociations = () => {
	debug('initializing course associations')
	initCourseAssociations(models)

	debug('initializing student associations')
	initStudentAssociations(models)
}

export const loadFixtures = async () => {
	const fixtures = [
		{ model: Course, file: './data/fixtures/courses.json' },
		{ model: Student, file: './data/fixtures/students.json' }
	]

	await Promise.all(
		fixtures.map((fixture) => {
			debug('loading fixtures for %s', fixture.model.name)
			const records = JSON.parse(fs.readFileSync(fixture.file).toString())

			return Promise.all(records.map((record) => fixture.model.upsert(record)))
		})
	)
}
