import { STATUS_CODES } from '../constants'

export const extendHeaders = (headers) => {
	if (!headers) {
		return
	}

	if (process.env.CORS_ORIGIN) {
		// eslint-disable-next-line no-param-reassign
		headers['Access-Control-Allow-Origin'] = process.env.CORS_ORIGIN
	}
}

export const reply = ({
	data,
	message,
	statusCode = STATUS_CODES.OK,
	headers = { }
} = { }) => {
	const body = message || (data && JSON.stringify(data))

	extendHeaders(headers)

	const response = { statusCode }

	if (body) {
		response.body = body
	}

	if (Object.keys(headers).length > 0) {
		response.headers = headers
	}

	return response
}
