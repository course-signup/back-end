import { Course } from '../models/course'
import { Student } from '../models/student'
import sendEmail from '../services/ses'
import { STATUS_CODES } from '../constants'

export const list = async () => {
	const courses = await Course.findAll({
		attributes: ['id', 'subject', 'seats', 'seatsAvailable'],
		include: {
			model: Student,
			attributes: ['id'],
			through: {
				attributes: []
			}
		}
	})

	return courses.map((course) => ({
		...course.dataValues,
		seatsAvailable: course.seatsAvailable,
		students: undefined
	}))
}

export const register = async ({
	courseId,
	email,
	matriculation
}) => {
	const student = await Student.findOne({
		where: {
			email,
			matriculation
		}
	})

	if (!student) {
		return {
			success: false,
			// This error could be more specific with a response
			// of a not authorized. However, this would have an
			// impact on the security of the service.
			statusCode: STATUS_CODES.NOT_FOUND
		}
	}

	const studentId = student.get('id')

	const course = await Course.findByPk(courseId, {
		include: {
			model: Student,
			attributes: ['id'],
			through: {
				attributes: []
			}
		}
	})

	if (!course) {
		return {
			success: false,
			statusCode: STATUS_CODES.NOT_FOUND,
			message: 'course not found'
		}
	}

	if (course.seatsAvailable === 0) {
		return {
			success: false,
			statusCode: STATUS_CODES.BAD_PARAMETERS,
			message: 'not seats for this course available'
		}
	}

	if (course.students.map(record => record.id).includes(studentId)) {
		return {
			success: true,
			message: 'already registered to course'
		}
	}

	await student.addCourse(course)

	await sendEmail({
		recipient: student.get('email'),
		message:
			`Hi ${student.get('name')},\n\n` +
			`you have been successfully registered to the course "${course.get('subject')}".\n\n` +
			'Regards\n' +
			'Course Signup Team',
		subject: 'Course Signup Confirmation'
	})

	return { success: true }
}
