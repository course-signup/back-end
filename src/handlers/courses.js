import Debug from 'debug'
import { STATUS_CODES } from '../constants'
import { list as listController, register as registerController } from '../controllers/courses'
import database from '../database'
import { reply } from '../helpers/response'

const debug = Debug('course-signup')

export const list = async () => {
	try {
		await database()

		const courses = await listController()

		return reply({ data: courses })
	} catch (error) {
		// eslint-disable-next-line no-console
		console.log('ERROR', error)
		debug('error while listing courses - %O', error)

		return reply({
			statusCode: STATUS_CODES.INTERNAL_SERVER_ERROR,
			message: error.message
		})
	}
}

export const register = async (event, context) => {
	try {
		context.callbackWaitsForEmptyEventLoop = false

		if (!event.body || !event.pathParameters) {
			return reply({ statusCode: STATUS_CODES.BAD_PARAMETERS })
		}

		const { courseId } = event.pathParameters
		const { email, matriculation } = JSON.parse(event.body)

		if (!courseId || !email || !matriculation) {
			return reply({ statusCode: STATUS_CODES.BAD_PARAMETERS })
		}

		await database()

		const {
			success,
			statusCode,
			message
		} = await registerController({
			courseId: parseInt(courseId, 10),
			email,
			matriculation: parseInt(matriculation, 10)
		})

		if (success !== true) {
			return reply({
				statusCode,
				message
			})
		}

		return reply({ message })
	} catch (error) {
		// eslint-disable-next-line no-console
		console.log('ERROR', error)
		debug('error while registering student for course - %O', error)

		return reply({
			statusCode: error.statusCode || STATUS_CODES.INTERNAL_SERVER_ERROR,
		})
	}
}
