# Course Signup - Back-end

Course Signup back-end which lets students list courses and register to them.

## Setup

- install / activate the correct Node.js version via `nvm install` / `nvm use`

- install all dependencies via `npm ci`

## Test

Test are developed with `jest` and can get started via `npm run test`.

## API

### List Courses

**Description:** list all available courses

**Path:** `/courses`

**Method:** GET

**Success Response Code:** 200 (OK)

**Response Schema:**

```
{
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type: "integer",
                "minimum": 1
            },
            "subject": {
                "type": "string"
            },
            "seats": {
                "type: "integer",
                "minimum": 1
            },
            "seatsAvailable": {
                "type: "integer",
                "minimum": 0
            }
        },
        "required": ["id", "subject", "seats", "seatsAvailable"]
    }
}
```

### Register Student to Course

**Description:** register a specific student to a course with available seats

**Path:** `courses/{courseId}`

**Method:** POST

**Success Response Code:** 200 (OK)

**Request Schema:**

```
{
    "type": "object",
    "properties": {
        "email": {
            "type: "string"
        },
        "matriculation": {
            "type: "integer"
        }
    },
    "required": ["email", "matriculation"]
}
```