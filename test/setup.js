// eslint-disable-next-line import/no-extraneous-dependencies
const YAML = require('yaml')
const fs = require('fs')
const path = require('path')

module.exports = () => {
	const config = YAML.parse(
		fs.readFileSync(path.join(__dirname, '../.env/env.yml')).toString(),
		{ merge: true }
	).test

	Object.keys(config).forEach((key) => {
		process.env[key] = config[key]
	})
}
