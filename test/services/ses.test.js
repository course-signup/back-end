import sendEmail from '../../src/services/ses'

const mockSESV2SendEmail = jest.fn(() => ({
	promise: () => new Promise(resolve => setTimeout(resolve, 100))
}))

jest.mock('aws-sdk', () => ({
	SESV2: jest.fn(() => ({
		sendEmail: mockSESV2SendEmail
	}))
}))

const processEnv = process.env

global.console = { log: jest.fn() }

beforeEach(() => {
	jest.resetModules()
	process.env = { ...processEnv }
})

afterEach(async () => {
	process.env = processEnv
	mockSESV2SendEmail.mockClear()
})

describe('services/ses: send email', () => {
	it('should send an email to the failsafe recipient when on dev stage', async () => {
		process.env.STAGE = 'dev'

		await sendEmail({
			recipient: 'test@test.com',
			subject: 'test',
			message: 'test2'
		})

		expect(mockSESV2SendEmail.mock.calls[0][0].Destination.ToAddresses[0])
			.toEqual(process.env.SES_FAILSAFE_RECIPIENT)
	})

	it('should send an email when on prod stage', async () => {
		process.env.STAGE = 'prod'

		const recipient = 'test@test.com'

		await sendEmail({
			recipient,
			subject: 'test',
			message: 'test2'
		})

		expect(mockSESV2SendEmail.mock.calls[0][0].Destination.ToAddresses[0])
			.toEqual(recipient)
	})
})
