import database from '../../src/database'
import { Course } from '../../src/models/course'
import { Student } from '../../src/models/student'

describe('models/course', () => {
	it('should return available seats correctly', async () => {
		await database()

		const course = await Course.findByPk(1, { include: Student })
		const student = await Student.findByPk(1)

		expect(course.get('seatsAvailable')).toEqual(course.getDataValue('seats'))

		await student.addCourse(course)
		await course.reload()

		expect(course.get('seatsAvailable')).toEqual(course.getDataValue('seats') - 1)

		await student.removeCourse(course)
	})

	it('should return no available seats in case something went wrong', async () => {
		await database()

		const course = await Course.findByPk(1, { include: Student })

		expect(course.get('seats')).toBeGreaterThan(0)
		expect(course.get('seatsAvailable')).toBeGreaterThan(0)

		const courseWithoutAssociation = await Course.findByPk(1)
		expect(courseWithoutAssociation.get('seats')).toBeGreaterThan(0)
		expect(courseWithoutAssociation.get('seatsAvailable')).toEqual(0)
	})
})
