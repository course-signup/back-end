import { STATUS_CODES } from '../../src/constants'
import { reply, extendHeaders } from '../../src/helpers/response'

const processEnv = process.env

beforeEach(() => {
	jest.resetModules()
	process.env = { ...processEnv }
})

afterEach(() => {
	process.env = processEnv
})

describe('helpers/response', () => {
	it('should return early on extending an undefined header', () => {
		expect(extendHeaders()).toBeUndefined()
	})

	it('should extend a header if enabled', () => {
		process.env.CORS_ORIGIN = '*'

		const headers = { }
		extendHeaders(headers)

		expect(headers['Access-Control-Allow-Origin']).toEqual('*')
	})

	it('should reply with a default response', () => {
		expect(reply()).toEqual({
			statusCode: STATUS_CODES.OK
		})
	})

	it('should stringify data as body', () => {
		const data = { test: 'value' }

		expect(reply({ data })).toEqual({
			statusCode: STATUS_CODES.OK,
			body: JSON.stringify(data)
		})
	})

	it('should correctly extend a header', () => {
		const data = { test: 'value' }

		expect(reply({ data, headers: { test: 123 } })).toEqual({
			statusCode: STATUS_CODES.OK,
			body: JSON.stringify(data),
			headers: { test: 123 }
		})
	})
})
