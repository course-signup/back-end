import { SEQUELIZE_DIALECTS } from '../../src/constants';
import database, { disconnect } from '../../src/database'

const processEnv = process.env

beforeEach(() => {
	jest.resetModules()
	process.env = { ...processEnv }
})

afterEach(async () => {
	process.env = processEnv
	await disconnect()
})

describe('database', () => {
	it('should reuse existing connections', async () => {
		const connection1 = await database()
		const connection2 = await database()

		expect(connection1).toStrictEqual(connection2)
	})

	it('should throw an error when unsupported sequelize dialect is used', async () => {
		process.env.SEQUELIZE_DIALECT = 'fake-dialect'
		await expect(() => database()).rejects.toThrow(new Error('unsupported or undefined sequelize dialect'))
	})

	it('should throw an error when no connection can be established', async () => {
		process.env.SEQUELIZE_DIALECT = SEQUELIZE_DIALECTS.MYSQL
		await expect(() => database()).rejects.toThrow(new Error('connect ECONNREFUSED 127.0.0.1:3306'))
	})
})
