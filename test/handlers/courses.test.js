/* eslint-disable no-console */
import { STATUS_CODES } from '../../src/constants'
import database, { disconnect } from '../../src/database'
import { list, register } from '../../src/handlers/courses'
import { Course } from '../../src/models/course'

const processEnv = process.env

global.console = { log: jest.fn() }

const email = 'flo@sfit.services'
const studentId = 1
const matriculation = 12345678
const courseId = 1

beforeEach(() => {
	jest.resetModules()
	process.env = { ...processEnv }
})

afterEach(() => {
	process.env = processEnv
})

afterAll(async () => {
	await disconnect()
})

describe('handlers/students:register for course', () => {
	it('should register a student for a course', async () => {
		const response = await register(
			{
				body: JSON.stringify({
					email,
					matriculation
				}),
				pathParameters: {
					courseId: '1'
				}
			},
			{ }
		)

		const course = await Course.findByPk(courseId)

		expect(response).toEqual({ statusCode: STATUS_CODES.OK })

		expect(console.log.mock.calls[0][0]).toContain('not sending email on purpose')
		expect(console.log.mock.calls[0][0]).toContain('no-reply@sfit.services')
		expect(console.log.mock.calls[0][0]).toContain('flo@sfit.services')
		expect(console.log.mock.calls[0][0]).toContain(`"${course.get('subject')}"`)

		await course.removeStudent(studentId)
	})

	it('should return an error response on missing context', async () => {
		const response = await register()
		expect(response).toEqual({ statusCode: STATUS_CODES.INTERNAL_SERVER_ERROR })
	})

	it('should return an error response on missing event', async () => {
		const response = await register(null, { })
		expect(response).toEqual({ statusCode: STATUS_CODES.INTERNAL_SERVER_ERROR })
	})

	it('should return an error response on missing body', async () => {
		const response = await register({ pathParameters: { } }, { })
		expect(response).toEqual({ statusCode: STATUS_CODES.BAD_PARAMETERS })
	})

	it('should return an error response on missing path parameters', async () => {
		const response = await register({ body: JSON.stringify({ }) }, { })
		expect(response).toEqual({ statusCode: STATUS_CODES.BAD_PARAMETERS })
	})

	it('should return an error response on missing email', async () => {
		const response = await register({
			body: JSON.stringify({ matriculation }),
			pathParameters: { courseId }
		}, { })

		expect(response).toEqual({ statusCode: STATUS_CODES.BAD_PARAMETERS })
	})

	it('should return an error response on missing matriculation', async () => {
		const response = await register({
			body: JSON.stringify({ email }),
			pathParameters: { courseId }
		}, { })

		expect(response).toEqual({ statusCode: STATUS_CODES.BAD_PARAMETERS })
	})

	it('should return an error response on incorrect courseId', async () => {
		const response = await register({
			body: JSON.stringify({ email, matriculation }),
			pathParameters: { courseId: 12345 }
		}, { })

		expect(response).toEqual({
			statusCode: STATUS_CODES.NOT_FOUND,
			body: 'course not found'
		})
	})

	it('should return an error response on incorrect email and matriculation', async () => {
		const response = await register({
			body: JSON.stringify({ email: 'none@test.com', matriculation }),
			pathParameters: { courseId: 12345 }
		}, { })

		expect(response).toEqual({
			statusCode: STATUS_CODES.NOT_FOUND
		})
	})

	it('should return an error response if no seats are available', async () => {
		const courseNoSeats = await Course.create({
			subject: 'Test Subject',
			seats: 1
		})

		await courseNoSeats.addStudent(studentId)

		const response = await register({
			body: JSON.stringify({ email, matriculation }),
			pathParameters: { courseId: courseNoSeats.get('id') }
		}, { })

		courseNoSeats.destroy()

		expect(response).toEqual({
			statusCode: STATUS_CODES.BAD_PARAMETERS,
			body: 'not seats for this course available'
		})
	})

	it('should not fail for an already registered student for a course', async () => {
		const course = await Course.create({
			subject: 'Test Subject',
			seats: 2
		})

		await course.addStudent(studentId)

		const response = await register({
			body: JSON.stringify({ email, matriculation }),
			pathParameters: { courseId: course.get('id') }
		}, { })

		await course.destroy()

		expect(response).toEqual({
			statusCode: STATUS_CODES.OK,
			body: 'already registered to course'
		})
	})
})

describe('handlers/courses:list', () => {
	it('should list all available courses', async () => {
		const response = await list()

		expect(response).toEqual({
			statusCode: STATUS_CODES.OK,
			body: JSON.stringify([
				{
					id: 1, subject: 'Math', seats: 15, seatsAvailable: 15
				},
				{
					id: 2, subject: 'English', seats: 25, seatsAvailable: 25
				},
				{
					id: 3, subject: 'Art', seats: 50, seatsAvailable: 50
				}
			])
		})
	})

	it('should respond with an internal server error on unexpected errors', async () => {
		await database()
		const response = await list()

		expect(response.statusCode).toEqual(STATUS_CODES.OK)

		await disconnect()

		jest.resetModules()
		process.env.SEQUELIZE_DIALECT = 'fake-dialect'

		const responseAfterDisconnect = await list()

		expect(responseAfterDisconnect.statusCode).toEqual(STATUS_CODES.INTERNAL_SERVER_ERROR)
	})
})
